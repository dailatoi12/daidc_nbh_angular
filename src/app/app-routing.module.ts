import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { WelcomComponent } from './welcom/welcom.component';
import { CustomerComponent } from './customer/customer.component';
import { MainComponent } from './layout/main/main.component';
const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'welcome', component: WelcomComponent},
  {path: 'customer', component: CustomerComponent},
  { 
  	path:'home',
  	component: MainComponent,
  	children: [
  	 { path: 'customer-manage', component: CustomerComponent}
  	]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {  constructor() { }

  ngOnInit(): void {}
}
